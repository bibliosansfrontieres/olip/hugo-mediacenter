#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') update_content.sh: $*"
}

say "Package updated: $1"
hugo-build
