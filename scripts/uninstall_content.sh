#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') uninstall_content.sh: $*"
}

say "Package removed: $1"

if [ -d "/data/$1" ];
then
    say "> Package $1 build found in /data/, rebuild mediacenter in ${workdir}"

    workdir='/tmp/hugo-build-workdir'

    # shellcheck disable=SC2164
    cd /data/
    mkdir -p "${workdir}"
    hugo-build "${workdir}"

    say "> Clean up previous website version"
    find . -maxdepth 1 \
        ! -name . -a ! -name .. \
        -a ! -name config.toml \
        -a ! -name content \
        -a ! -name themes \
        -a ! -name olip_index.json \
        -exec rm -rf '{}' \;

    say "> Deploy the new website version"
    cp -rf "${workdir}"/* /data/
    rm -rf "${workdir}"
else
    say "> Package $1 build not found, nothing to do"
fi
