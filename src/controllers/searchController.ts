'use strict';

var searchService = require('../services/searchService');
var OpenSearchModel = require('../models/OpensearchModel');
var encoder = require('../encoder/openSearchSerializer');
//var buildJson = require('../services/buildJsonWithOthersJson')
import { buildJsonWithOthersJson } from '../services/workers/buildJsonWithOthersJson'
const PATH = '/api/mediacenter/v1.0';

module.exports = function(app) {
let number = 0;

  app.get(PATH + '/search/:keyword', function(req, res){
    console.time('internal search')
    searchService.search(req.params.keyword)
      .then((results) => res.send(results));
    console.timeEnd('internal search');
  });

  app.get(PATH + '/opensearch/:keyword', function(req, res){
    searchService.search(req.params.keyword)
      .then(
        (results) => {
          //console.log(encoder.toOpenSearchFormat(req.params.keyword,results));
          res.send(encoder.toOpenSearchFormat(req.params.keyword,results));
        }
      );
  });

  app.get(PATH + '/opensearch_descriptor/', function(req, res){
    res.header('Content-Type','text/xml').send(encoder.toXMLOpenSearchDescriptor(OpenSearchModel.OpenSearchDescriptor));
  });

  //Not Implemented
  app.get(PATH + '/search/:field/:keyword', function(req, res){
    /*
      searchService.search(req.params.keyword)
        .then((results) => res.send(results));
    */
  });

  //Not Implemented
  app.get(PATH + '/search', function(req, res){

  });

  //Not Implemented
  app.get(PATH + '/search/:language/:field/:keyword', function(req, res){
  });

}
 