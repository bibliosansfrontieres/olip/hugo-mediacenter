# Contributing to Media center

## Contributing with a new merge resquest

If you are about to make a Merge Request,
here is some things you should care as Test are not automatized yet:

* Search modal should render items based on search term
* Opensearch with OLIP should work as well
* The mediacenter should not rebuild at installation
* The mediacenter should rebuild when a file change in `/data/content/`
* The mediacenter should be able to install new package
* `olip_index.json` should be generated at startup
* `olip_index.json` should be generated at each change in `/data/content`
* Metrics about build time should be logged out
* Sitemap must stay deactivated

### Home

* Traductions should be available
* Breadcrumb should be present
* Images should be rendered
* Media's icons should be rendered
* Media types count should match

### Package page

* JS language's filter should be here for multilingual packages
* Images should be rendered for thumbnail
* There should be no link to others relative content
* There should be a title for each package item
* A description of the package should be present at the top

### Item page

* Each resources must be downloadable (`pdf`/`jpg`/`mp4`/`mp3`)

## Creating a release

* Update and commit the [Changelog][Changelog) file with relevant changes
* Update the version field in the catalog `descriptor.json` file
* Create and push a git tag:

```shell
export TAG=v1.0.0 && git tag -a -s -m "Release $TAG" "$TAG" && git push origin "$TAG"
```
