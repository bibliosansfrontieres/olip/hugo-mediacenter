# hugo-mediacenter

[![pipeline status](https://gitlab.com/bibliosansfrontieres/olip/hugo-mediacenter/badges/master/pipeline.svg)](https://gitlab.com/bibliosansfrontieres/olip/hugo-mediacenter/commits/master)

A Mediacenter for [OLIP](https://gitlab.com/bibliosansfrontieres/olip).

## File tree

### |- api

Contains the code for the Express server API
to serve the opensearch end point for OLIP.
See [Implementing the search API](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/blob/master/app-developer.rst)
for more informations about specifications

### |- hugo-config

Contains

* `config.toml` Move at container start in \`/data/
* `themes/my_themes`  Move at container start in `/data/`

### |- test

Contain the test for the API

## Content format

Each item will be stored in a package. The media center will be constitued of packages.

Each package of items must use this format.

```text
apprendre-a-utiliser-des-patrons-fr
├── Comment_reproduire_un_patron.mp4
├── Comment_reproduire_un_patron.mp4.md
├── Comment_reproduire_un_patron.mp4.png
├── index.json
├── Reporter_un_patron_sur_tissu.mp4
├── Reporter_un_patron_sur_tissu.mp4.md
└── Reporter_un_patron_sur_tissu.mp4.png
```

the command to zip the folder is the following

```shell
cd apprendre-a-utiliser-des-patrons-fr
zip -r apprendre-a-utiliser-des-patrons-fr.zip *
```

Markdown will follow something similar to :

```text
$ cat Comment_reproduire_un_patron.mp4.md
+++
date = "2019-02-12T13:18:17+04:00"
title = "Comment réaliser un patron à partir d'un vêtement ?"
draft = false
image = "content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4.png"
showonlyimage = false
weight = 2
+++
<video width="80%" height="auto" controls>
    <content src="../../content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4" type="video/mp4">
</video>

Cette vidéo montre aux tailleurs comment réaliser un patron à partir d'un vêtement assemblé. Il prend l'exemple d'un t-shirt, mais peut être généralisé à tout type de vêtement.
```

Please note the **path** for

* image: `image = "content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4.png"`
* content: `<content src="../../content/apprendre-a-utiliser-des-patrons-fr/Comment_reproduire_un_patron.mp4" type="video/mp4">`

## CICD

Each `push` on `master` branch will trigger a new build of the Docker image.

Images:

* [DockerHub](https://hub.docker.com/u/offlineinternet)
* [GitLab Registry](https://gitlab.com/bibliosansfrontieres/olip/apps/hugo-mediacenter/container_registry)

### Re-build image

The Docker image can be re-build with:

```shell
docker build -f Dockerfile . -t offlineinternet/hugo-mediacenter:latest
```

And ran this way:

```shell
docker run -ti --name hugo --rm -p 8080:3000 -v /tmp/data/:/data offlineinternet/hugo-mediacenter:latest
```

## Hugo development

This method of developing is the closest way to stick to the production version

### Clone the repository

```shell
git clone git@gitlab.com:bibliosansfrontieres/olip/hugo-mediacenter.git
cd hugo-mediacenter
```

### Download some content

```shell
mkdir data/content/
cd data/content/
wget http://packages.ideascube.org/hugo/fight-against-discriminations-and-live-together-en.zip
mkdir fight-against-discriminations-and-live-together-en
cd fight-against-discriminations-and-live-together-en
unzip ../fight-against-discriminations-and-live-together-en.zip

```

### Launch the container

```shell
cd hugo-mediacenter
docker run -it --name hugo --rm -p 80:3000 \
    --entrypoint "/docker/entrypoint-dev.sh" \
    -v $(pwd)/hugo-config/config.toml:/data/config.toml \
    -v $(pwd)/hugo-config/themes/:/data/themes \
    -v $(pwd)/data/content:/data/content/ \
    offlineinternet/hugo-mediacenter sh
```

At this point Hugo (in the container) build the website from the source based on

* the themes `hugo-config/themes/`
* the content `data/content`
* using the config file `config.toml`

The whole thing is stored in `/data/` and served by `Node JS Express webserver`

Once launch the container will show you

```text
[nodemon] 1.19.4
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node app.js`
Media center appplication RESTful API server started on: 3000
```

Hit Enter and you'll get the prompt back

After modifying the sources launch Hugo again to build the website

```shell
cd /data/ && /usr/local/bin/hugo --config /data/config.toml -d /data/
```

Result are available on : <http://localhost>

## Ressources

### Search

* <https://fr.jeffprod.com/blog/2018/un-moteur-de-recherche-interne-pour-votre-site-hugo/>
* <https://codepen.io/JMChristensen/pen/PpweZP>
* <https://lunrjs.com/>
* <https://gitlab.com/kaushalmodi/hugo-search-fuse-js>
