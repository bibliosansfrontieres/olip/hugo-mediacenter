var constants = require('../models/constants');
var fs = require('fs'), lunr = require('lunr'), xmljs = require('xml-js');
var util = require('util');
var path = require('path');
var SearchModel = require('../models/OpensearchModel');
/**
private functions
*/
function getData() {
    return new Promise(function (resolve, reject) {
        fs.readFile(constants.public_folder + '/content/olip_index.json', 'utf8', function (err, content) {
            return err ? reject(err) : resolve(content);
        });
    });
}
function parseJson(rawData) {
    return new Promise(function (resolve, reject) {
        return resolve(rawData);
    });
}
function searchTerms(json, terms) {
    var results = [];
    var idx = lunr(function () {
        this.ref('id');
        this.field('title', { boost: 10 });
        this.field('tags');
        this.field('href');
        this.field('description');
        json = JSON.parse(json);
        json.forEach(function (entry) {
            this.add(entry);
        }, this);
    });
    idx.search(terms).forEach(function (occurrence) {
        var r = json.find(function (obj) {
            return obj.id === parseInt(occurrence.ref);
        });
        for (var key in r) {
            if (key === 'href') {
                var urlFormat = r[key].split('/').slice(1).join('/').replace(/\.[^/.]+$/, "");
                r[key] = urlFormat;
            }
        }
        results.push(r);
    });
    console.log(results);
    return results;
}
exports.search = function (terms) {
    return getData()
        .then(function (rawData) { return parseJson(rawData); })
        .then(function (json) { return searchTerms(json, terms); })
        .catch(function (error) { return console.log(error); });
};
