"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.buildJsonWithOthersJson = void 0;
var fs = require('fs');
var buildJsonWithOthersJson = /** @class */ (function () {
    function buildJsonWithOthersJson(path, descriptor) {
        this.path = path;
        this.descriptor = descriptor;
    }
    buildJsonWithOthersJson.prototype.readDirectory = function () {
        return fs.readdirSync(this.path);
    };
    buildJsonWithOthersJson.prototype.readFilesInAllDirectory = function (files) {
        var source = null;
        for (var file in files) {
            var pathFinal = this.path + '/' + files[file];
            if (fs.lstatSync(pathFinal).isDirectory())
                source += fs.readdirSync(pathFinal);
        }
        return source;
    };
    buildJsonWithOthersJson.prototype.strcmp = function (search, string) {
        return ((search == string) ? 0 : ((search > string) ? 1 : -1));
    };
    buildJsonWithOthersJson.prototype.checkForJsonInSources = function (sources) {
        var json;
        var object = sources.split(",");
        var pattern = 'index.json';
        for (var value in object) {
            if (this.strcmp(object[value], pattern) == 0) {
                json = object[value];
            }
        }
        return "/" + json;
    };
    buildJsonWithOthersJson.prototype.writeAJsonFileFromAllFolders = function (json, files, objects) {
        json = "/index.json";
        var tmp;
        var output = " ";
        var final = " ";
        for (var object in objects) {
            tmp = this.path + '/' + objects[object] + json;
            if (fs.existsSync(tmp)) {
                final += output + fs.readFileSync(tmp, "utf-8");
            }
        }
        return final;
    };
    buildJsonWithOthersJson.prototype.jsonValidator = function (object) {
        var regex = /\] \[/g;
        object = object.replace(regex, ',');
        return object;
    };
    buildJsonWithOthersJson.prototype.buildFinalObject = function (object) {
        var finalPath = this.path + "/" + this.descriptor;
        return fs.writeFileSync(finalPath, object, 'utf-8');
    };
    buildJsonWithOthersJson.prototype.start = function () {
        if (fs.existsSync(this.path)) {
            var files = this.readDirectory();
            var dataInFiles = this.readFilesInAllDirectory(files);
            var getJsonObject = this.checkForJsonInSources(dataInFiles);
            var buildJson = this.writeAJsonFileFromAllFolders(getJsonObject, dataInFiles, files);
            var result = this.jsonValidator(buildJson);
            var objectFinal = this.buildFinalObject(result);
            fs.chmod(this.path + '/' + this.descriptor, '0777', function (err, data) {
                if (err)
                    console.log(err);
                else
                    console.log("chmod set");
            });
        }
        else
            console.log('no data');
        if (!(fs.existsSync(this.path + '/' + this.descriptor))) {
            fs.openSync(this.path + '/' + this.descriptor, 'w');
            fs.chmod(this.path + '/' + this.descriptor, '0777', function (err, data) {
                if (err)
                    console.log(err);
                else
                    console.log("chmod set");
            });
        }
        return objectFinal;
    };
    return buildJsonWithOthersJson;
}());
exports.buildJsonWithOthersJson = buildJsonWithOthersJson;
